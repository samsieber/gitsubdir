# GitSubDir

This tool is for exposing a subdirectory of a git repository (refered to 'upstream' for the rest of the document) as it's own synchornized repository ('subdirgit' for the rest of the document).

It sets up some git hooks and tracks commit meta-data to keep the two repos in sync. 

The main advantage to this setup over others, is that one does not need to learn about git submodules or subtrees in order to use the main repository.

## Requirements / Know Limitations

The upstream repository and subdirgit repository must exist on the same machine, so that we can symlink them. 

Commit order between the two trees are inconsistent - there is no work around for it, 
The author date may be right, but the commit date won't be. So comparing graphs may be a little wonky.

The setup script places a hook in the subdirgit and optionally in the upstream. 
It is flexible enough to handle custom hook directories, but not custom hook names.

This is a "update" based hook implementation - that means the hook is called once per branch pushed, not once per push action. 
The hook is also in python and limits itself to single, sequential execution, potentially limiting throughput for very large repositories.

While in general each type of scenario encountered during the synchronization process has been tested, there may be edge cases unaccounted for. Use with caution

## Synchronization Scenarios

Anticipated scenarios and how they are handled:

 * Empty Commits: These are not copied between the repos, unless they are merge commits
 * Commits with content: A diff is generate for the diff, and translated to fit the other repo. 
 It is then applied and committed using the source commitss author, message body and date.
 * Merge commits: This triggers a merge between the corresponding parents in the dest repo. 
 * Merge conflict: This is resolved by resetting the state to that of the parent, 
 applying the diff from the merge commit, 
 and the committing using the source commit's author, message body and date.
 * Branch deletion: Deletes the corresponding branch in the other repository
 * Branch creation: Creates the branch in the corresponding commits and imports any new commits

## Usage

Clone this repo. Run ./setup.py -h for usage instructions. Typical usage:

```./setup.py path/to/upstream.git path/to/fresh/or/non-existent/mirror.git target/folder/in/upstream/to/mirror```

Run ```./setup.py -h``` for additional options, such as specifying a different data or hook directory.

## Testing

To test, run testdata/general/test.sh. It will create 4 repositories under build/general.
 * upstream - the upstream working repo
 * subgit - the mirror working repo - it's mirroring the "target" directory of upstream
 * upstream.git & subgit.git - the bare versions of the above

In addition, it will run some tests and report any failures. (either synchronization differences or hook failures)

### Comparing

The test script programmatically compares the logs and file state of the two git repos. See testdata/general/compare.sh for how.
 
## How it Works

Setup creates five repositories inside of a data directory (usually inside the gitsubdir .git folder).
 * upstream.git - this is a symlink to the upstream repo. It's used for generating commits
 * upstream - this is a working (e.g. not bare) clone of the upstream
 * local.git - this is a bare repo whose content (all save HEAD, hooks/ and the data directory) are symlinked to the corresponding content in the mirror
 * local - the working clone of local.git (not the mirror). 
 We point at local.git instead of the mirror so that we don't trigger hooks when pushing.
 * map - a local only working git repo, used to track the upstream <-> mirror commit mapping.


Setup runs a git command/script which makes the bare repository a mirror of the upstream. 
It then rewrites the history (locally) to move the target subdirectory to the root. 
While doing so, it builds a mapping of upstream <-> mirror commit. 
It then removes the backups generated during the rewrite and flushes those objects with git-gc and git-reflog

It then copies the hook (main.py) into the hook directory (as update), and places a data file there to specify the location of the data directory.
It also copies lib.py to the hooks directory, so that the update hook can use it. It's a separate file so that it gets compile by python, and so that the setup script can reuse some logic.

### Upstream.git

This repository is a symlink to the upstream repository.

### Upstream

This repository is used for pushing new from the mirror repo to upstream. 

### Local.git

This repository is almost identical to the mirror repository. Its content is a little different:
 * The packed_refs, FETCH_HEAD, config, description, info, logs, objects, refs files/directories are symlinks to the same objects in the mirror
 * HEAD is omitted because symlinking it breaks git - instead it is copied since it never changes
 * hooks/ is an empty directory

This repository exists so that when importing commits from the upstream, 
commits can be pushed to the mirror without triggering another update hook.

While it might be possible to use the plumbing of git to avoid a solution like this, 
this seemed like the easiest solution.

### Local

This to push commits into the local.git repository when copying commits from upstream. 
It clones local.git and not the mirror directly to avoid triggering hooks.

### Map

This repo is locally created and contains a mapping of upstream<->local commit.
The mapping is stored on disk as a key-value store. 
Given a sha, one can look up the corresponding upstream or mirror file which contains the sha it maps to in the other repository.
It's a repo so that if we run into errors while copying commits, we can reset the state back to the HEAD.

## Advanced Git Usage

This project uses many git commands or options that are outside of day-to-day usage (least common to most common):
 * Using git-filter-tree for the initial import. The --tree-filter option was used to move the directory instead of subdirectory filter so that it could run on all commits. The --commit-filter was used to record the old sha vs new sha, and prune empty commits.
 * Expring the original refs and gc'ing them, and accounting for the difference in repo structure pre and post packing.
 * Symlinking most of a git repository, but not all, because git can't handle it.
 * Removing the GIT_DIR env variable, which inside the hook. That's overrideable by the -C option, but not by setting the cwd.
 * Using git-merge with --no-commit --no-ff along with other commands to always perform a merge commit with the right changes and parents.
 * Generating patches for parts of repository by passing "-- specific/part" to git diff.
 * Using the --pretty option to extract the committer author, date and message from a commit, and specifying them via switches.
 * Pushing a sha to remote branch, instead of a local branch.
 * Using rev-parse to get the current sha, and to detect repository presense
