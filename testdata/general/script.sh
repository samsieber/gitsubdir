#!/usr/bin/env bash
eval top=$homedir"~/dev/gitsubdir/build/general"
rm -rf $top || true
mkdir -p $top

UPSTREAM="$top/upstream"
SUBGIT="$top/subgit"
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
BASE="$PWD"
LOG="$top/logs"
LOG_FILE=""
SECTION=""
sec_i=0

echo "$UPSTREAM"
echo "$SUBGIT"

mkdir -p "$LOG"

output_to_file(){
    exec 3>&1 4>&2 >"$1" 2>&1
}

restore_output_to_tty(){
    exec 1>&3 2>&4
}

start_test(){
    printf -v SEC_I "%02d" $sec_i
    SECTION="$1"
    let sec_i+=1
    echo "Starting Test : '$SECTION'"
    LOG_FILE="$LOG/${SEC_I}_$SECTION"
    output_to_file "$LOG_FILE"
}

finish_test(){
    if [ ! -z "$SECTION" ]
    then
        restore_output_to_tty
        echo "Test Successful: '$SECTION'"
        SECTION=""
    fi
}

section(){
    finish_test
    start_test "$1"
}

compare() {
    # $1 is the branch
    "$DIR/compare.sh" "$UPSTREAM.git" "$SUBGIT.git" target $1 || exit $?
}

section "setup-upstream" 
{
    git init $UPSTREAM
    git -C $UPSTREAM config push.default simple

    git init --bare $UPSTREAM.git
    git -C $UPSTREAM remote add origin "file://$UPSTREAM.git"

    git -C $UPSTREAM am $DIR/0001-add-README.patch
    git -C $UPSTREAM am $DIR/0002-Update-readme.patch
    git -C $UPSTREAM am $DIR/0003-Create-subdirectory-a.patch

    echo "dot test" > "$UPSTREAM/.test"
    git -C $UPSTREAM add .
    git -C $UPSTREAM commit -m "Testing dot stuff"

    git -C $UPSTREAM am $DIR/0004-Add-more-stuff-to-a.patch
    git -C $UPSTREAM push -u origin master

    git -C $UPSTREAM reset --hard HEAD~
    git -C $UPSTREAM am $DIR/0005-This-will-trigger-a-merge-commit.patch
    git -C $UPSTREAM fetch origin master
    git -C $UPSTREAM merge origin/master -m "Merge branch 'master' of 'upstream'"

    git -C $UPSTREAM tag tag1

    git -C $UPSTREAM push origin tag1

    git -C $UPSTREAM checkout -b no-sub
    git -C $UPSTREAM am $DIR/0006-Removing-sub-dir.patch
    git -C $UPSTREAM push -u origin no-sub

    git -C $UPSTREAM checkout master
    git -C $UPSTREAM am $DIR/0007-Add-more-to-a.patch

    git -C $UPSTREAM checkout -b feature
    git -C $UPSTREAM am $DIR/0008-Adding-stuff-in-a-feature-branch.patch
    git -C $UPSTREAM push -u origin feature

    git -C $UPSTREAM checkout master
    git -C $UPSTREAM am $DIR/0009-Added-stuff-in-main-branch.patch
    git -C $UPSTREAM merge feature -m "Merge branch 'feature'"


    git -C $UPSTREAM am $DIR/0010-Adding-sud-directory.patch

    git -C $UPSTREAM tag contains-first
    git -C $UPSTREAM push origin contains-first

    git -C $UPSTREAM am $DIR/0011-Toplevel-work.patch
    git -C $UPSTREAM am $DIR/0012-More-target-work.patch
    git -C $UPSTREAM am $DIR/0013-Test-moving-files-between-workspaces.patch
    git -C $UPSTREAM am $DIR/0014-Revert-Test-moving-files-between-workspaces.patch

    git -C $UPSTREAM checkout -b test-t
    git -C $UPSTREAM push -u origin test-t
    git -C $UPSTREAM checkout master
    git -C $UPSTREAM push 
}

section "setup-local-bare"
( ./setup.py $UPSTREAM.git $SUBGIT.git target -s -l "DEBUG"    ) || exit $?

section "import-non-relevant-export-single"
{
    git init $SUBGIT
    git -C $SUBGIT config push.default simple

    echo "Parent2" > $UPSTREAM/merge-conflict.txt
    git -C $UPSTREAM add .
    git -C $UPSTREAM commit -m "Parent2"
    git -C $UPSTREAM push
    git -C $UPSTREAM reset --hard HEAD~
    echo "Parent1" > $UPSTREAM/merge-conflict.txt
    git -C $UPSTREAM add .
    git -C $UPSTREAM commit -m "Parent1"
    git -C $UPSTREAM pull
    echo "Resolved" > $UPSTREAM/merge-conflict.txt
    git -C $UPSTREAM add .
    git -C $UPSTREAM commit -m "Merged w/resolution"
    git -C $UPSTREAM push

    git -C $SUBGIT remote add origin "file://$SUBGIT.git"
    git -C $SUBGIT fetch --all
    git -C $SUBGIT checkout master
    git -C $SUBGIT branch --set-upstream-to=origin/master master
    git -C $SUBGIT pull

    echo "Testing" >> $SUBGIT/TRACKED.md
    git -C $SUBGIT add .
    git -C $SUBGIT commit -m "Testing commit from subdir"
    git -C $SUBGIT push || exit $1

    compare master
}


section "delete-local-and-upstream-branch"
{
    git -C $SUBGIT push origin :feature || exit $1

    compare master
}

section "push-local-branch"
{
    git -C $SUBGIT checkout master
    git -C $SUBGIT checkout -b new-branch
    echo "Testing" >> $SUBGIT/BRANCH_FILE.md
    git -C $SUBGIT add .
    git -C $SUBGIT commit -m "Testing pushing a new commit"
    (git -C $SUBGIT push -u origin new-branch) || exit $?

    git -C $SUBGIT checkout master

    compare new-branch
    
}

section "push-local-branch-atop-new-upstream-no-new"
{
    git -C $UPSTREAM checkout master
    git -C $UPSTREAM checkout -b new-upstream-branch
    git -C $UPSTREAM push -u origin new-upstream-branch
    git -C $UPSTREAM checkout master

    git -C $SUBGIT checkout master
    git -C $SUBGIT checkout -b new-upstream-branch
    git -C $SUBGIT push -u origin new-upstream-branch
    git -C $SUBGIT checkout master

    git compare new-upstream-branch
}

section "export-another-single"
{
    git -C $UPSTREAM pull

    echo "Testing" >> $SUBGIT/TRACKED.md
    git -C $SUBGIT add .
    git -C $SUBGIT commit -m "Testing another commit from subdir"
    git -C $SUBGIT push || exit $?

    compare master
}

section "export-another-single"
{
    echo "Testing" >> $SUBGIT/UNTRACKED.md
    git -C $SUBGIT add .
    git -C $SUBGIT commit -m "Testing yet another commit from subdir"
    git -C $SUBGIT push

    compare master
}

section "test-import-relevant-merge"
{
    echo "New from upstream" > $UPSTREAM/target/new-upstream.txt
    git -C $UPSTREAM add .
    git -C $UPSTREAM commit -m "Commit from upstream"
    git -C $UPSTREAM fetch
    git -C $UPSTREAM merge origin/master -m "Merge branch 'master' of origin-upstream"
    git -C $UPSTREAM push



    echo "Testing" >> $SUBGIT/new-local.txt
    git -C $SUBGIT add .
    git -C $SUBGIT commit -m "Testing from subdir"
    git -C $SUBGIT push

    res=$?

    if [ $res -eq 0 ]
    then 
        echo "Push succeeded, but should have failed: $res"
        exit 1
    fi

    compare master
}

section "test-import-relevant-merge-successful"
{
    # TODO check that the above push command failed
    # It was needed however to trigger an import
    git -C $SUBGIT fetch
    git -C $SUBGIT merge origin/master -m "Merge branch 'master' of origin-subdir"
    git -C $SUBGIT push

    compare master
}

section "test-cron-update-call"
{

    git -C $UPSTREAM checkout master
    git -C $UPSTREAM pull
    echo "New from upstream" > $UPSTREAM/target/cron-update-master.txt
    git -C $UPSTREAM add .
    git -C $UPSTREAM commit -m "New upstream branch commit for cron on master"
    git -C $UPSTREAM push

    "$SUBGIT.git/hooks/update" || exit $?

    compare master
}


section "test-cron-new-branch-call"
{

    git -C $UPSTREAM checkout master
    git -C $UPSTREAM pull
    git -C $UPSTREAM checkout -b new-cron-branch
    echo "New from upstream" > $UPSTREAM/target/new-cron-branch.txt
    git -C $UPSTREAM add .
    git -C $UPSTREAM commit -m "New upstream branch commit for cron on master"
    git -C $UPSTREAM push -u origin new-cron-branch

    "$SUBGIT.git/hooks/update" || exit $?

    compare new-cron-branch
}

section "test-cron-new-branch-call-no-new"
{
    git -C $UPSTREAM checkout new-cron-branch
    git -C $UPSTREAM pull
    git -C $UPSTREAM checkout -b new-cron-branch-no-new
    git -C $UPSTREAM push -u origin new-cron-branch-no-new

    "$SUBGIT.git/hooks/update" || exit $?

    compare new-cron-branch-no-new
}

section "test-cron-delete-call"
{

    git -C $UPSTREAM checkout new-cron-branch-no-new
    git -C $UPSTREAM push origin :new-cron-branch-no-new

    "$SUBGIT.git/hooks/update" || exit $?
}

section "test-cron-do-all-call"
{
    # Push a branch w/new stuff
    git -C $UPSTREAM checkout new-cron-branch-no-new
    echo "New from upstream" > $UPSTREAM/target/new-cron-branch-no-new.txt
    git -C $UPSTREAM add .
    git -C $UPSTREAM commit -m "New upstream branch commit for cron on new-cron-branch-no-new (jk)"
    git -C $UPSTREAM push origin new-cron-branch-no-new:new-cron-branch-no-new

    # Delete a branch
    git -C $UPSTREAM push origin :new-cron-branch

    # Update a branch (empty though...)
    git -C $UPSTREAM checkout master
    echo "New master stuff" > $UPSTREAM/README.md
    git -C $UPSTREAM add .
    git -C $UPSTREAM commit -m "New upstream branch commit that doesn't apply to subdir"
    git -C $UPSTREAM push origin master


    "$SUBGIT.git/hooks/update" || exit $?

    compare master
    compare new-cron-branch-no-new
}

section "add-post-receive-hook"
{
    rm -rf $SUBGIT
    ( ./setup.py -c $UPSTREAM.git $SUBGIT.git target -s -l "DEBUG" -p hooks/  ) || exit $?
    git init $SUBGIT
    git -C $SUBGIT config push.default simple
    git -C $SUBGIT remote add origin "file://$SUBGIT.git"
    git -C $SUBGIT fetch --all
    git -C $SUBGIT checkout master
    git -C $SUBGIT branch --set-upstream-to=origin/master master
    git -C $SUBGIT pull
}

section "delete-branch-upstream-post-receive"
{
    git -C $UPSTREAM checkout master
    git -C $UPSTREAM push origin :new-cron-branch-no-new || exit $?
}
section "push-branch-upstream-post-receive"
{
    git -C $UPSTREAM push origin new-cron-branch-no-new:refs/heads/new-hook-branch || exit $?

    sleep 1
    compare new-hook-branch
}
section "push-update-upstream-post-receive"
{
    git -C $UPSTREAM echo "From upstream..." > "$UPSTREAM/target/new-hook-file.txt"
    git -C $UPSTREAM add .
    git -C $UPSTREAM commit -m "Upstream update for hook"
    git -C $UPSTREAM push
    
    sleep 1
    compare master
}
finish_test


echo "All tests successful"

echo "Subgit Commits"
git -C build/general/subgit.git log --pretty="%at %an %s" -- "" | sort -g
echo "Upstream Commits to target"
git -C build/general/upstream.git log --pretty="%at %an %s" -- target | sort -g





