#!/usr/bin/env bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
eval TOP=$homedir"~/dev/gitsubdir/build/general"

"$DIR/script.sh"

status=$?

if [ $status -ne 0 ]
then
    latest=$(cd "$TOP/logs"; ls -tr | sort | tail -n 1);

    echo "FAILED ON SECTION $latest :"

    cat "$TOP/logs/$latest" | sed 's/^/  /'


    echo "FAILED ON SECTION ^^^^ $latest ^^^^"
    exit $status
fi