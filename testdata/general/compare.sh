#!/usr/bin/env bash

UPSTREAM="$1"
SUBDIR="$2"
TARGET="$3"
BRANCH="$4"

# Create temp dir (OSX and linux compatible)
TEMPDIR=`mktemp -d 2>/dev/null || mktemp -d -t 'mytmpdir'`

# We need a working copies of the git repos, yet don't want to mess up existing ones, in case this comparison happens mid-test
UPSTREAM=`cd "$UPSTREAM"; pwd`
SUBDIR=`cd "$SUBDIR"; pwd`

git clone "file://$UPSTREAM" "$TEMPDIR/upstream" &> /dev/null
git clone "file://$SUBDIR" "$TEMPDIR/subdir" &> /dev/null

UPSTREAM="$TEMPDIR/upstream"
SUBDIR="$TEMPDIR/subdir"

git -C "$SUBDIR" checkout "$BRANCH" &> /dev/null
git -C "$UPSTREAM" checkout "$BRANCH" &> /dev/null

# Compare the logs
diff <(git -C "$UPSTREAM" log --pretty="%at %an %s" -- "$TARGET" | sort -g) <(git -C "$SUBDIR" log --pretty="%at %an %s" -- "" | sort -g)
log_diff=$?
if [ $log_diff -eq 0 ]
then
    echo "No log differences"
fi

# Compare the files
diff -Nqr -x '.git' "$UPSTREAM/$TARGET" "$SUBDIR" 
file_diff=$?
if [ $file_diff -eq 0 ]
then
    echo "No file differences"
fi

# Cleanup
rm -rf "$TEMPDIR"

if [ $log_diff -ne 0 ]
then
    exit $log_diff
fi

if [ $file_diff -ne 0 ]
then
    exit $file_diff
fi