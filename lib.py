#!/usr/bin/env python

# This file exists so that the hook logic can be mostly executed
# Additionally, it allows the setup file to reuse some of this logic during setup

import os, sys, shutil, subprocess, shlex, uuid
from collections import namedtuple

#
# Logging
#

import logging
logger = logging.getLogger('mirror-lib')

def get_logging():
    return logger, ch, fh


#
# File Utilities
#

def mkfile(abs_path, content, **kwargs):
    if not os.path.exists(os.path.dirname(abs_path)):
        os.makedirs(os.path.dirname(abs_path))
    write_content(abs_path, content)

def write_content(path, content):
    with open(path, 'w') as output_file:
        output_file.write(content)

def content_of(path):
    with open(path, 'r') as input_file:
        return input_file.read().strip()

def content_of_with_fallback(path, fallback=""):
    if exists(path):
        return content_of(path)
    else:
        return fallback

def exists(path):
    return os.path.exists(path)


#
# SHA Utilities
#

NO_SHA = "0"*40

def content_of_sha(path):
    return content_of_with_fallback(path, fallback=NO_SHA)

# Return a relative nested directory path for a commit sha, 1 character per folder for *depth* folders
def sha_path(sha, depth=4):
    return sha if depth == 0 else sha[0] + "/" + sha_path(sha[1:], depth=depth-1)

def get_current_sha(abs_repo_dir):
    return sh('git rev-parse HEAD', cwd=abs_repo_dir).stdout.strip()


#
# Command Execution
#

CmdResult = namedtuple('CmdResult', ['returncode', 'stdout', 'stderr'], verbose=False)

def sh(arg_string, **kwargs):
    arg_list = shlex.split(arg_string)
    return do_cmd(arg_list, **kwargs)
    

def do_cmd(args, cwd=None, debug=False, input=None, error_on_fail=True, env={}, stdout=subprocess.PIPE, stderr=subprocess.PIPE, stdin=subprocess.PIPE):
    my_env = os.environ.copy()
    if "GIT_DIR" in my_env:
        del my_env["GIT_DIR"]
    for key in env.keys():
        my_env[key] = env[key]
    logger.debug("%s %s", args, cwd)
    proc = subprocess.Popen(args, cwd=cwd, stdout=stdout, stderr=stderr, stdin=stdin, env=my_env)
    out, err = proc.communicate(input)
    logger.debug(out)
    logger.debug(err)
    res = CmdResult(proc.returncode, out, err)
    if error_on_fail and res[0] is not 0:
        raise Exception("Command \"%s\" failed with error code: %s\nWorking dir: %s\nStandard Out:%s\nStandard Error:%s" % (' '.join(args), res.returncode, cwd, res.stdout, res.stderr))
    return res

#
# Single File Execution
#

def lock_until_finished(path):
    import fcntl
    handle=open(path,'r')
    try:
        fcntl.flock(handle,fcntl.LOCK_EX|fcntl.LOCK_NB)
    except:
        sys.exit(1)
    return handle

#
# Git Ref Utils
#

def ref_to_sha(abs_repo_dir, ref_name):
    res = sh("git show-ref -s '%s'" % ref_name, cwd=abs_repo_dir, error_on_fail=False)
    if res.returncode == 0:
        return res.stdout.strip()
    else:
        return NO_SHA

def ref_to_branch(ref):
    return ref.split("/")[-1]


#
# Git Repo Misc. Utils
#

def translate_diff(content, source, dest):
    def fix(suffix):
        return "" if len(suffix) == 0 else suffix + "/"
    lha = ' a/' + fix(source)
    lhb = ' b/' + fix(source)
    rha = ' a/' + fix(dest)
    rhb = ' b/' + fix(dest)
    return content.replace(lha, rha).replace(lhb, rhb)

def is_in_merge(abs_repo_dir):
    return sh("git merge HEAD", cwd=abs_repo_dir, error_on_fail=False).returncode == 128

def is_an_ancestor_of(abs_repo_dir, potential_ancestor, parent):
    res =  sh('git merge-base --is-ancestor %s %s' % (potential_ancestor, parent), cwd=abs_repo_dir, error_on_fail=False)
    if res.returncode == 0:
        return True
    if res.returncode == 1:
        return False
    raise Exception(res.stderr + res.stdout)

class RepoSource:
    def __init__(self, data_dir, name, sha_map_dirname, target):
        self.data_dir = data_dir
        self.name = name
        self.working = self.data_dir + "/" + name
        self.bare = self.data_dir + "/" + name + ".git"
        self._sha_map_dirname = sha_map_dirname
        self.target = target

    def translate_sha(self, sha):
        return os.path.join(self.data_dir, "map", self._sha_map_dirname, sha_path(sha))

    def get_ref_sha_map(self):
        lines = sh("git show-ref --heads", error_on_fail=False, cwd=self.bare).stdout.strip().split("\n")
        if len(lines) == 1 and len(lines[0]) == 0:
            return {}
        else:
            return { ref : sha for (sha, ref) in [ entry.split() for entry in lines ] }

# 
#  Commit Visit Order
#

# This one is in this grouping because the visit list construction depends on receiving the list reverse topological order from git
def git_rev_list(*args, **kwargs):
    if 'error_on_fail' not in kwargs:
        kwargs['error_on_fail'] = True
    cmd_args = ["git","rev-list","--reverse","--topo-order","--parents"] + list(args)
    output = do_cmd(cmd_args, **kwargs).stdout.strip()
    return (line.split() for line in output.split("\n") if len(line) > 0)

def convert_to_visit_list(repo_source, commits):
    return (commit_and_parents_list for commit_and_parents_list in commits if not exists(repo_source.translate_sha(commit_and_parents_list[0])))

def make_tuples_unique_by_idx_0(items):
    unique_checker = {}
    unique_by_0 = []
    for key,value in items:
        if key not in unique_checker:
            unique_checker[key] = True
            unique_by_0.append((key,value))
    return unique_by_0


def get_diff(source, parent, commit, dest):
    diff = sh('git diff %s %s -- %s' % (parent, commit, "./"+dest.target), cwd=source.bare).stdout
    return translate_diff(diff, dest.target, source.target)

def get_commit_meta_switches(abs_repo_path, commit):
    date_switch=sh('''git log -1 --pretty=format:"--date=%ad" ''' + commit, cwd=abs_repo_path).stdout.strip()
    author_switch =sh('''git log -1 --pretty=format:"--author=%an <%ae>" ''' + commit, cwd=abs_repo_path).stdout.strip()
    message_switch =sh('''git log -1 --pretty=format:"-m%B" ''' + commit, cwd=abs_repo_path).stdout.strip()
    info_switches = [date_switch, author_switch, message_switch]
    
    committer_email=sh('''git log -1 --pretty=format:%ce ''' + commit, cwd=abs_repo_path).stdout.strip()
    committer_name=sh('''git log -1 --pretty=format:%cn ''' + commit, cwd=abs_repo_path).stdout.strip()
    config_switches = ["-c", "user.name=" + committer_name, "-c", "user.email=" + committer_email]

    return config_switches, info_switches

def make_orphaned_commit(abs_path_repo, commit_action):
    temp_branch_name = str(uuid.uuid4())+str(uuid.uuid4())
    sh('git checkout --orphan %s' % temp_branch_name, cwd=abs_path_repo)
    sh('git reset --hard', cwd=abs_path_repo)
    sh('git clean -xfd', cwd=abs_path_repo)
    commit_action()
    head = get_current_sha(abs_path_repo)
    sh('git checkout %s' % head, cwd=abs_path_repo)
    sh('git branch -D %s' % temp_branch_name, cwd=abs_path_repo)
    return head

# This is the bulk of the logic
# Handles the following
# 1) Empty commits
# 2) Merge commits (arbitrary number of parents)
# 3) n ... m parent mapping (eg some parents map to the same commit)
# 4) Empty merge commits
def copy_commit(source, dest, commit_and_parents_list):
    commit = commit_and_parents_list[0]
    parents = commit_and_parents_list[1:]
    logger.info("Applying commit %s from %s to %s; parents: %s:", commit, source.name, dest.name, parents)

    config_switches, info_switches = get_commit_meta_switches(source.bare, commit)

    if len(parents) == 0:
        patch_content = sh("git format-patch -1 --stdout %s -- '%s'" % (commit, dest.target), cwd=source.bare).stdout
        if len(patch_content) > 0:
            patch_content = translate_diff(patch_content, dest.target, source.target)
            apply_patch = lambda: do_cmd(['git'] + config_switches + ['am'], cwd=dest.working, input=patch_content)
            head = make_orphaned_commit(dest.working, apply_patch)
        else:
            head = ref_to_sha(dest.working, "refs/sync/empty")

        mkfile(dest.translate_sha(head), commit)
        mkfile(source.translate_sha(commit), head)

        return head 

    dest_parents_tuples = make_tuples_unique_by_idx_0((content_of(source.translate_sha(parent)),parent) for parent in parents)
    dest_parents = [dest_parent for (dest_parent, parent) in dest_parents_tuples]
    parents = [parent for (dest_parent, parent) in dest_parents_tuples]
    diff_content = get_diff(source, parents[0], commit, dest)


    if len(dest_parents) == 1:
        sh('git checkout %s' % dest_parents[0], cwd=dest.working)
        if len(diff_content) > 0:
            logger.debug("Diff Content: %s", diff_content)
            sh('git apply', cwd=dest.working, input=diff_content, debug=True)
            sh('git add .', cwd=dest.working, debug=True)
            do_cmd(['git']+config_switches+['commit']+info_switches, cwd=dest.working, debug=True, error_on_fail=True) 

            head = get_current_sha(dest.working)
            logger.info("Copied commit %s (%s in dest) to %s (%s in dest)" , commit, head, parents[0], dest_parents[0])
        else: 
            logger.info("Diff Content: Empty")

            head = dest_parents[0]
            logger.info("Commit %s was empty. Updated head (%s) accordingly.", commit, head)
    else:
        if is_an_ancestor_of(dest.working, dest_parents[1], dest_parents[0]):
            dest_parents[0], dest_parents[1] = dest_parents[1], dest_parents[0]
            parents[0], parents[1] = parents[1], parents[0]
        sh('git checkout %s' % dest_parents[0], cwd=dest.working)
        merge_result = do_cmd(['git']+config_switches+['merge','--no-commit']+info_switches[2:]+dest_parents[1:], cwd=dest.working, debug=True, error_on_fail=False)
        if is_in_merge(dest.working):
            sh('git reset -q HEAD -- .', cwd=dest.working)
            sh('git checkout HEAD -- .', cwd=dest.working)
            sh('git clean -fd', cwd=dest.working)
            sh('git apply', cwd=dest.working, input=diff_content)
            sh('git add .', cwd=dest.working)
            do_cmd(['git']+config_switches+['commit', '--allow-empty']+info_switches, cwd=dest.working, error_on_fail=True)

            head = get_current_sha(dest.working)
            logger.info("Copied commit %s (%s in dest) to %s (%s in dest)", commit, head, parents, dest_parents)
        elif merge_result.returncode == 0:
            head = get_current_sha(dest.working)
        else:
            raise Exception("Unexpected state: Merge command failed.\nStdOut:%s\nStdErr:%s" % (merge_result.stdout, merge_result.stderr))


    mkfile(dest.translate_sha(head), commit)
    mkfile(source.translate_sha(commit), head)

    return head 

def is_standard_ref(ref_name):
    # We don't copy tags across repos - live with it
    return ref_name.startswith("refs/heads/") or ref_name == "HEAD"

class MirrorRepo:
    def __init__(self, data_dir, target, commit_sub_num=10):
        self.target = target
        self.data_dir = data_dir

        self.upstream = RepoSource(self.data_dir, "upstream", "upstream_to_local", "")
        self.local = RepoSource(self.data_dir, "local", "local_to_upstream", self.target)

        self.commit_sub_num = commit_sub_num

    def _get_commits_to_import_list(self, old_upstream_sha, new_upstream_sha):
        # Try preemptively exclude commits that have already been mapped
        # So, map recent commits in the local to commits in the upstream, and exclude their ancestors
        # The ^ before a commit means exclude their ancestors
        if old_upstream_sha == NO_SHA:
            recent_local_commits = sh('git log -n %s --all --format=format:%%H' % self.commit_sub_num, cwd=self.local.bare, error_on_fail=False, debug=True).stdout.strip().split()
            recent_imported_upstream_commits = [content_of(self.local.translate_sha(local_sha)) for local_sha in recent_local_commits]

            git_rev_list_args = [new_upstream_sha] + ["^%s" % upstream_commit for upstream_commit in recent_imported_upstream_commits]

            return git_rev_list(*git_rev_list_args, cwd=self.upstream.bare)
        else:
            return git_rev_list("%s..%s" % (old_upstream_sha, new_upstream_sha), cwd=self.upstream.bare)

    def import_upstream_commits(self, ref_name, old_upstream_sha, new_upstream_sha):
        if old_upstream_sha == new_upstream_sha:
            logger.info("Aborting import for %s", ref_name)
            return

        commits = self._get_commits_to_import_list(old_upstream_sha, new_upstream_sha)
        to_visit = convert_to_visit_list(self.upstream, commits)

        sh("git fetch --all", cwd=self.local.working)

        new_sha = NO_SHA
        for commit_and_parents_list in to_visit:
            new_sha = copy_commit(self.upstream, self.local, commit_and_parents_list)
        if new_sha == NO_SHA:
            new_sha = content_of(self.upstream.translate_sha(new_upstream_sha))

        sh("git checkout %s" % new_sha, cwd=self.local.working)
        sh("git checkout -B %s" % ref_to_branch(ref_name), cwd=self.local.working)
        sh("git push origin %s:%s" % (new_sha, ref_name), cwd=self.local.working)
        logger.info("Imported " + new_upstream_sha + " from upstream")
        self.save("Imported " + new_upstream_sha + " from upstream")

    # NOTE: It's pretty easy to get out of sync with the remote repo...
    def export_local_commits(self, ref_name, old_local_sha, new_local_sha):
        sh("git fetch --all", cwd=self.upstream.working)


        if old_local_sha == NO_SHA:
            commits = git_rev_list(new_local_sha, cwd=self.local.bare)
        else:
            commits = git_rev_list("%s..%s" % (old_local_sha, new_local_sha), cwd=self.local.bare)
        to_visit = convert_to_visit_list(self.local, commits)

        new_sha = NO_SHA
        for commit_and_parents_list in to_visit:
            new_sha = copy_commit(self.local, self.upstream, commit_and_parents_list)
        if new_sha == NO_SHA:
            new_sha = content_of(self.local.translate_sha(new_local_sha))

        push_res = sh("git push origin %s:%s" % (new_sha, ref_name), cwd=self.upstream.working, debug=True, error_on_fail=False)

        if push_res.returncode > 0:
            print "Could not sync with upstream server - please try again."
            print push_res.stdout, push_res.stderr
            logger.warn("Could not sync with upstream server: %s %s %s\nCmd:git push origin %s:%s\nStdOut:%s\nStdErr:%s", ref_name, old_local_sha, new_local_sha, new_sha, ref_name, push_res.stdout, push_res.stderr)
            self._abort()

        logger.info("Exported " + new_local_sha + " to upstream")
        self.save("Exported " + new_local_sha + " to upstream")

    def delete_upstream_ref(self, ref_name):
        upstream_sha = ref_to_sha(self.upstream.bare, ref_name)
        if not upstream_sha == NO_SHA:
            upstream_result = sh('git push origin :%s' % ref_name, cwd=self.upstream.working, error_on_fail=False)
            if upstream_result.returncode > 0:
                logger.warn("Could not remove upstream branch: %s\StdOut:%s\nStdErr:%s", ref_name, upstream_result.stdout, upstream_result.stderr)
                print upstream_result.stdout, upstream_result.stderr
                self._abort()
        
    def delete_local_ref(self, ref_name):
        local_sha = ref_to_sha(self.local.bare, ref_name)
        if not local_sha == NO_SHA:
            local_result = sh('git push origin :%s' % ref_name, cwd=self.local.working, error_on_fail=False)
            if local_result.returncode > 0:
                logger.warn("Could not remove upstream branch: %s\StdOut:%s\nStdErr:%s", ref_name, local_result.stdout, local_result.stderr)

    def _abort(self, exit=True):
        self.restore()
        if exit:
            sys.exit(1)


    def restore(self):
        sh("git reset --hard HEAD", cwd=self.data_dir+"/map")
        sh("git clean -xfd", cwd=self.data_dir+"/map")

    def save(self, message):
        sh("git add .", cwd=self.data_dir+"/map")
        if len(sh("git status --porcelain", cwd=self.data_dir+"/map").stdout.strip()) > 0:
            sh("git commit -m '%s'" % message, cwd=self.data_dir+"/map")

    def update_ref_from_upstream(self, ref_name):
        self.restore()
        if not is_standard_ref(ref_name):
            sys.exit(0)

        upstream_branch_sha = ref_to_sha(self.upstream.bare, ref_name)
        local_branch_sha = ref_to_sha(self.local.bare, ref_name)
        old_upstream_sha = content_of_sha(self.local.translate_sha(local_branch_sha))

        if upstream_branch_sha == NO_SHA:
            self.delete_local_ref(ref_name)
        else:
            self.import_upstream_commits(ref_name, old_upstream_sha, upstream_branch_sha)

    def run_update_all_from_upstream(self):
        self.restore()
        upstream_ref_sha_map = self.upstream.get_ref_sha_map()
        local_ref_sha_map = self.local.get_ref_sha_map()

        upstream_keys = (ref_name for ref_name in upstream_ref_sha_map.keys() if is_standard_ref(ref_name))

        for ref_name in upstream_keys:
            upstream_branch_sha = upstream_ref_sha_map.pop(ref_name)

            local_branch_sha = local_ref_sha_map.pop(ref_name,NO_SHA)
            old_upstream_sha = content_of_sha(self.local.translate_sha(local_branch_sha))

            logger.info("ref_name: " + ref_name)
            logger.info( "upstream: " + upstream_branch_sha)
            logger.info( "local:" + local_branch_sha)
            logger.info( "old upstream:" + old_upstream_sha)

            if old_upstream_sha != upstream_branch_sha:
                self.import_upstream_commits(ref_name, old_upstream_sha, upstream_branch_sha)

        for ref_name in local_ref_sha_map.keys():
            self.delete_local_ref(ref_name)


    def run_update_hook(self, ref_name, old_sha, new_sha):
        self.restore()
        # We only sync on branches + HEAD, not tags or other non-standard refs
        if not is_standard_ref(ref_name):
            sys.exit(0)

        # Deleting a branch
        if new_sha == NO_SHA:
            self.delete_upstream_ref(ref_name)
            sys.exit(0)

        old_upstream_sha = content_of_sha(self.local.translate_sha(ref_to_sha(self.local.bare, ref_name)))
        actual_upstream_sha = ref_to_sha(self.upstream.bare, ref_name)

        # If we're out of date with upstream, we should import the commits
        if not old_upstream_sha == actual_upstream_sha:
            self.import_upstream_commits(ref_name, old_upstream_sha, actual_upstream_sha)

        # If for some reason we've changed the local current branch during the script, abort
        # TODO: change this to ancestor check using git-merge-base
        if not ref_to_sha(self.local.bare, ref_name) == old_sha:
            logger.info("Rejecting %s because it is behind %s", old_sha, ref_to_sha(self.local.bare, ref_name))
            print "Your branch is out of date. Please re-pull."
            self._abort()

        logger.info("Going to accept commits")
        self.export_local_commits(ref_name, old_sha, new_sha)
