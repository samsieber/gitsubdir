#!/usr/bin/env bash
REMOTE_PATH=%s

zero_commit="0000000000000000000000000000000000000000"

while read oldrev newrev refname; do
    # Do this async, because each call has the potential to trigger this very script
    nohup "$REMOTE_PATH" "$refname" &
done

exit 0