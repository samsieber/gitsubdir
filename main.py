#!/usr/bin/env python

# This is a server side update hook

import os, sys, logging
from lib import content_of, lock_until_finished, MirrorRepo, logger, sh

def config_logging(data_dir, level):
    fh = logging.FileHandler(os.path.join(data_dir, "logs", "log.txt"))
    fh.setFormatter(logging.Formatter(fmt="%(asctime)s %(levelname)s %(message)s"))
    logger.addHandler(fh)
    logger.setLevel(getattr(logging,level))

def main():
    # Ban concurrent execution of the rest of the script
    file_path = os.path.abspath(sys.argv[0])
    handle = lock_until_finished(file_path)

    # Find the data file
    script_dir = os.path.dirname(file_path)
    git_dir = sh("git rev-parse --git-dir", cwd=script_dir).stdout.strip()

    # Setup
    data_dir,target,level = content_of(os.path.join(git_dir, 'data.txt')).split(",")
    config_logging(data_dir, level)
    mirror_repo = MirrorRepo(data_dir, target)


    try:
        # Extract Args
        args = sys.argv[1:]
        if len(args) == 0:
            mirror_repo.run_update_all_from_upstream()
        elif len(args) == 1:
            ref_name = args[0]
            mirror_repo.update_ref_from_upstream(ref_name)
        elif len(args) == 3:
            ref_name, old_sha, new_sha = args
            mirror_repo.run_update_hook(ref_name, old_sha, new_sha)
        else:
            raise Exception("Wrong number of arguments: expected 0, 1 or 3; instead received %s: %s" % (len(args), args))
    except Exception:
        mirror_repo._abort(exit=False)
        raise

if __name__ == "__main__":
    main()
