#!/usr/bin/env python

from lib import sh, do_cmd, sha_path, mkfile, logger, content_of ,content_of_with_fallback, is_standard_ref, MirrorRepo, get_commit_meta_switches, make_orphaned_commit
import shlex, shutil
import os, sys
import argparse
import logging

def to_abs(path):
    return os.path.abspath(path)

def to_absolute_path(root, path_frag):
    if os.path.isabs(path_frag):
        return path_frag
    else:
        return os.path.join(root, path_frag)

class BareGitRepoSupport:
    def __init__(self, path):
        self.path = path

    def _is_inited(self):
        if not os.path.exists(self.path):
            return False
        answer = sh("git rev-parse --is-inside-git-dir", cwd=self.path, error_on_fail=False).stdout
        return answer == "true"

    def create_if_needed(self):
        if not self._is_inited():
            sh("mkdir -p '%s'" % self.path, debug=True)
            sh("git init --bare",cwd=self.path, debug=True) 

class SubDirBareGitRepo(BareGitRepoSupport): 
    def __init__(self, path, data_dir="data"):
        self.path = path
        self.data_dir = to_absolute_path(self.path, data_dir)

    def create_data_dirs(self):
        sh("mkdir '%s'" % self.data_dir)
        # create dir for logs
        sh("mkdir '%s/logs'" % self.data_dir)

def run_setup(subgit_dir, upstream_subgit_dir, sub_dir, relative_hook_dir="hooks", data_dir="/data", symlink_scripts=True, log_level="INFO", generate_upstream_post_receive=None, clean_before=False):
    logger.info("Setting up git subdirectory mirror")
    logger.info( "Upstream git file location: " + upstream_subgit_dir)
    logger.info( "Mirror repository location: " + subgit_dir)
    logger.info( "Target directory to mirror: " + sub_dir)

    subgit = SubDirBareGitRepo(to_abs(subgit_dir), data_dir=data_dir)
    upstream = BareGitRepoSupport(to_abs(upstream_subgit_dir))


    subgit_hook_dir = to_absolute_path(subgit.path, relative_hook_dir)
    map_dir = subgit.data_dir+"/map"

    if clean_before:
        logger.info("Removing old data...")
        del_paths = []
        if not generate_upstream_post_receive == None:
            upstream_post_receive_filename = os.path.join(to_absolute_path(upstream.path, generate_upstream_post_receive), "post-receive")
            del_paths.append(upstream_post_receive_filename)
        del_paths = del_paths + [os.path.join(subgit_hook_dir, "update"), os.path.join(subgit_hook_dir, "lib.py"), subgit.data_dir]
        for path in del_paths:
            if os.path.exists(path):
                logger.info("Removing old file/folder: %s", path)
                sh("rm -rf %s" % path)

        refs = sh('git show-ref', cwd=subgit.path).stdout.strip()
        if len(refs) > 0:
            for line in refs.split("\n"):
                sha, ref = line.split()
                logger.info("Removing old ref: %s", ref)
                sh('git update-ref -d %s' % ref, cwd=subgit.path)

    subgit.create_if_needed()
    subgit.create_data_dirs()

    logger.info("Creating the upstream<->mirror repo")
    sh("mkdir map", cwd=subgit.data_dir)
    sh("git init", cwd=map_dir)
    sh("git add .", cwd=map_dir)
    sh("git commit --allow-empty -m 'Initial state'", cwd=map_dir)

    logger.info("Creating upstream bare access (symlink)")
    sh("ln -s %s %s/upstream.git" % (upstream.path, subgit.data_dir))
    
    logger.info("Creating upstream working directory (for moving changes from subdir -> upstream)")
    sh("git clone file://%s upstream" % upstream_subgit_dir, cwd=subgit.data_dir)
    
    logger.info("Creating mirror bare access (using symlinks, but excluding hooks)")
    unprotected_subgit_dir = subgit.data_dir + "/local.git"
    sh("mkdir -p " + unprotected_subgit_dir)
    # Symlink everything except hooks and HEAD
    for subgit_item in shlex.split("config description info logs objects refs packed-refs"):
        sh("ln -s %s/%s %s/%s" % (subgit.path, subgit_item, unprotected_subgit_dir, subgit_item))
    # Git fails if HEAD is a symlink
    sh('cp %s/%s %s/%s' % (subgit.path, "HEAD", unprotected_subgit_dir, "HEAD"))
    # And we don't want a copy of the symlink in our repo 
    sh("mkdir %s/hooks" % unprotected_subgit_dir)

    logger.info("Create mirror working directory (for moving changes from upstream -> subdir)")
    sh("git clone file://%s local" % unprotected_subgit_dir, cwd=subgit.data_dir)

    logger.info("Setting up the mirror 'update' hook")
    # Copy the hook into the right place
    src = os.path.dirname(os.path.abspath(__file__))
    if symlink_scripts:
        sh("ln -s %s/main.py %s/update" % (src,subgit_hook_dir))
        sh("ln -s %s/lib.py %s/lib.py" % (src,subgit_hook_dir))
    else:
        shutil.copyfile(src+"/main.py", subgit_hook_dir+"/update")
        shutil.copyfile(src+"/lib.py", subgit_hook_dir+"/lib.py")
    sh("chmod +x %s/update" % subgit_hook_dir, debug=True, error_on_fail=True) 

    logger.info("Generating the mirror 'update' hook configuration")
    data_file = subgit.path + "/data.txt"
    with open(data_file, 'w') as data_file_handle:
        data_file_handle.write(to_absolute_path(subgit.path, subgit.data_dir)+","+sub_dir+","+log_level)

    logger.info("Adding a general purpose empty base commit.")
    base_commit = sh("git rev-list --all --reverse --date-order --max-parents=0", cwd=subgit.data_dir+"/upstream.git").stdout.split()[0]
    empty_committer_info, empty_author_info = get_commit_meta_switches(subgit.data_dir+"/upstream.git", base_commit)
    make_empty_commit = lambda: do_cmd(["git"]+empty_committer_info+["commit", "--allow-empty"]+empty_author_info, cwd=subgit.data_dir+"/local")
    empty_orphan = make_orphaned_commit(subgit.data_dir+"/local", make_empty_commit)
    sh("git update-ref refs/sync/empty %s" % empty_orphan, cwd=subgit.data_dir+"/local")

    logger.info("Running update hook to import upstream commits.")
    data_dir,target,level = content_of(data_file).split(",")
    mirror_repo = MirrorRepo(data_dir, target)
    mirror_repo.run_update_all_from_upstream()

    if not generate_upstream_post_receive == None:
        logger.info("Adding async upstream 'post-receive' server hook")
        post_receive_content = content_of(os.path.join(src, "upstream-post-receive.sh")) % os.path.join(subgit_hook_dir, "update")
        upstream_post_receive_filename = os.path.join(to_absolute_path(upstream.path, generate_upstream_post_receive), "post-receive")
        mkfile(upstream_post_receive_filename, post_receive_content)
        sh("chmod +x '%s'" % upstream_post_receive_filename)


def main_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("upstream_repo", help="Upstream bare git repo path")
    parser.add_argument("target_repo", help="The path of the bare repo to convert into a subdir mirror")
    parser.add_argument("target_path", help="The path of the folder in the upstream to mirror in the target_repo")
    parser.add_argument("-k", "--hook-dir", dest="hooks_dir", default="hooks", help="The path of the hook folder in the target_repo. Defaults to hooks.")
    parser.add_argument("-d", "--data-dir", dest="data_dir", default="data", help="The path of the data folder in the target_repo. Defaults to data.")
    parser.add_argument("-s", "--symlink-scripts", dest="symlink_scripts", action='store_true', help="Symlink the update script. False by default.")
    parser.add_argument("-l", "--git-log-level", dest="log_level", default="INFO", help="The python logging level to use when recording log files. Defaults to INFO. Possible values are DEBUG, INFO.")
    parser.add_argument("-v", "--setup-log-level", dest="script_log_level", default="INFO", help="The python logging level to use running this script. Defaults to INFO")
    parser.add_argument("-p", "--upstream-post-receive", dest="upstream_post_receive", default=None, help="The relative hook path to create the upstream post receive hook in. If omitted, no upstream post-receive hook to sync is created.")
    parser.add_argument("-c", "--pre-clean", dest="clean_before", action="store_true", help="The relative hook path to create the upstream post receive hook in. If omitted, no upstream post-receive hook to sync is created.")
    return parser

def main():
    parser = main_parser()
    args = parser.parse_args()

    logger.addHandler(logging.StreamHandler())
    logger.setLevel(getattr(logging, args.script_log_level))

    if getattr(logging, args.log_level) is None:
        raise Exception("An invalid log level %s was specified" % args.log_level)

    run_setup(
        args.target_repo, 
        args.upstream_repo, 
        args.target_path, 
        relative_hook_dir=args.hooks_dir, 
        symlink_scripts=args.symlink_scripts,
        data_dir=args.data_dir,
        log_level=args.log_level,
        generate_upstream_post_receive=args.upstream_post_receive,
        clean_before=args.clean_before
        )

if __name__ == "__main__":
    main()